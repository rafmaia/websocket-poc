import { TimerService } from './services/timer.service';
import { Component, OnInit } from '@angular/core';
import { Timer } from './models/timer.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private timer: TimerService) {}
  timer$: Observable<Timer> = new Observable();

  ngOnInit(): void {
    this.timer$ = this.timer.timer$;
  }

  title = 'socket-app';
}
