import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Timer } from 'src/app/models/timer.model';

@Injectable({
  providedIn: 'root',
})
export class TimerService {
  timer$ = this.socket.fromEvent<Timer>('data');

  constructor(private socket: Socket) {}
}
